function write_to_file(path, text)
{
    var f, date;

    f = new File(path, "readwrite", "TEXT");
    if(f.isopen)
    {
        f.position = f.eof;
        f.writeline(text);
    }
    f.close();
}

function check_new_text(text)
{
	if( typeof check_new_text.prev == 'undefined' )
 	{
        check_new_text.prev = " ";
    }

	if (check_new_text.prev != text)
	{
		check_new_text.prev = text;
		return true;
	}
}

function csvstring_from_arguments()
{
	var string = "";
	var i;
	for (i=0; i < arguments.length - 1; i++) 
	{
		string += arguments[i] + ','
	}
	string += arguments[i]
	return string
}

function write()
{

	var dateGetter = new Date();
	var isodate = dateGetter.toISOString();
	if( typeof write.date == 'undefined' )
 	{
        write.date = isodate.split("T")[0]

    }

    timestamp = dateGetter.toLocaleTimeString()

	if( typeof write.start_time == 'undefined' )
 	{

        write.start_time = dateGetter.toLocaleTimeString()

    }


	
	switch(this.inlet)
	{

		case 0:
		 	f = new File(f_r, "readwrite", "TEXT");
		 	if(f.isopen)
    	 	{
				var line = csvstring_from_arguments(arguments[0], arguments[1], arguments[2], write.date, arguments[3], arguments[4], arguments[5], arguments[6], write.start_time, timestamp, arguments[7], arguments[8], arguments[9], arguments[10], arguments[11], arguments[12], arguments[13]);
				f.position = f.eof;
        		f.writeline(line);
    	 	}
    	 	f.close();
		 	break;
		
		case 1:
		 	f = new File(f_p, "readwrite", "TEXT");
		 	if(f.isopen)
    	 	{        		

				var line = csvstring_from_arguments(arguments[0], arguments[1], arguments[2], write.date, arguments[3], arguments[4], arguments[5], arguments[6], write.start_time, timestamp, arguments[7], arguments[8], arguments[9], arguments[10], arguments[11], arguments[12], arguments[13], arguments[14], arguments[15], arguments[16], arguments[17] );
				f.position = f.eof;
        		f.writeline(line);
    	 	}
    	 	f.close();
		 	break;
		
		case 2:
		 	f = new File(f_h, "readwrite", "TEXT");
		 	if(f.isopen)
    	 	{
				var line = csvstring_from_arguments(arguments[0], arguments[1], arguments[2], write.date, arguments[3], arguments[4], arguments[5], arguments[6], write.start_time, timestamp, arguments[7], arguments[8], arguments[9], arguments[10]);
				f.position = f.eof;
        		f.writeline(line);
    	 	}
    	 	f.close();
		 	break;
		 	break;
	}
	
}

function create_files(path, SubjectCode, ConditionCode)
{
	

	var dateGetter = new Date();

	var isodate = dateGetter.toISOString()
    date = isodate.split("T")[0]
	start_time = dateGetter.toLocaleTimeString()

	
	
	if( typeof create_files.r == 'undefined' )
 	{	
		create_files.r = false;
		create_files.p = false;
		create_files.h = false;

    }
	
	file_start_time = start_time.replace(/[., :]/g, "-");
	
	switch(this.inlet)
	{
		case 0:
			f_r = path + "/" + SubjectCode + "_" + ConditionCode + "_" + date + "_" + file_start_time + "_Results.csv";
		 	f = new File(f_r, "readwrite", "TEXT");
		 	if(f.isopen && !create_files.r)
    	 	{
          		f.position = f.eof;
        		f.writeline("Subject, Age, Sex, Date, CondCode, SNR, trial_p_condition, trial_p_session, start_time, time_stamp, Wave, W1, W2, W3, W4, W5, total, revs");
				//create_files.r = true;
    	 	}
    	 	f.close();
		 	break;
		
		case 1:

		 	f_p = path + "/" + SubjectCode + "_" + ConditionCode + "_" + date + "_" + file_start_time + "_Pupildata.csv";
			
		 	f = new File(f_p, "readwrite", "TEXT");
		 	if(f.isopen && !create_files.r)
    	 	{
          		f.position = f.eof;
        		f.writeline("Subject, Age, Sex, Date, CondCode, SNR, trial_p_condition, trial_p_session, start_time, time_stamp, left_size_active, left_size, left_gaze_valid, left_x, left_y, right_size_active, right_size, right_gaze_valid, right_x, right_y, event");
				//create_files.p = true;
    	 	}
    	 	f.close();
		 	break;
		
		case 2:

		 	f_h = path + "/" + SubjectCode + "_" + ConditionCode + "_" + date + "_" + file_start_time + "_Headtracking.csv";
	 	
		 	f = new File(f_h, "readwrite", "TEXT");
		 	if(f.isopen && !create_files.r)
    	 	{
          		f.position = f.eof;
        		f.writeline("Subject, Age, Sex, Date, CondCode, SNR, trial_p_condition, trial_p_session, start_time, time_stamp, pitch, yaw, roll, event");
				//create_files.h = true;
    	 	}
    	 	f.close();
		 	break;
	}
}

var f_r, f_h, f_p
var start_time
autowatch = 1

inlets = 3
outlets = 0
