
var test_info = null

// returns or includes null if there is a dict without containing data.
function dict_to_jsobj(dict) {
	if (dict == null) return null;
	var o = new Object();
	var keys = dict.getkeys();
	if (keys == null || keys.length == 0) return null;

	if (keys instanceof Array) {
		for (var i = 0; i < keys.length; i++)
		{
			var value = dict.get(keys[i]);
			
			
			
			if (value && value instanceof Dict) 
			{
				value = dict_to_jsobj(value);

			}

			o[keys[i]] = value;
		}		
	} else {
		var value = dict.get(keys);
		
		if (value && value instanceof Dict) {
			value = dict_to_jsobj(value);
		}
		o[keys] = value;
	}

	return o;
}

function jsobj_to_dict(o) {
	var d = new Dict();
	
	for (var keyIndex in o)	{
		var value = o[keyIndex];

		if (!(typeof value === "string" || typeof value === "number")) {
			var isEmpty = true;
			for (var anything in value) {
				isEmpty = false;
				break;
			}
			
			if (isEmpty) {
				value = new Dict();
			}
			else {
				var isArray = true;
				for (var valueKeyIndex in value) {
					if (isNaN(parseInt(valueKeyIndex))) {
						isArray = false;
						break;
					}
				}
			
				if (!isArray) {
					value = jsobj_to_dict(value);
				}
			}
		}
		d.set(keyIndex, value);
	}
	return d;
}

// this function will post a JS object's content to the max window
function printobj (obj, name) {
    post("---- object " + name + "----" +"\n");
    printobjrecurse(obj, name);
}

function printobjrecurse (obj, name) {
    if (typeof obj === "undefined") {
        post(name + " : undefined" +"\n");
        return;
    }
    if (obj == null) {
        post(name + " : null" +"\n");
        return;
    }

    if ((typeof obj == "number") || (typeof obj == "string")) {
        post(name +  " :" + obj + "\n");
    } else {
        var num = 0;
        for (var k in obj) {
            if (obj[k] && typeof obj[k] == "object")
            {
                printobjrecurse(obj[k], name + "[" + k + "]");
            } else {
                post(name + "[" + k + "] : " + obj[k] +"\n")
            }
            num++;
        }
        if (num == 0) {
            post(name + " : empty object" +"\n");
        }
    }
}

function print()
{
	load()
	printobj(test_info)
}

function load()
{
	if (test_info == null)
	{
		var d = new Dict("test_info")
		test_info = dict_to_jsobj(d)
	}
}

function getSetting()
{
	load()
	var obj = test_info
	for (arg in arguments)
	{
		//post(arg)
		for (var k in obj)
		{
			if (k == arguments[arg])
			{
            	if (obj[k] && typeof obj[k] == "object")
				{
					obj = obj[k]
					break;
				}
				else
				{
					post(obj[k])
				}
			}
				
		}
	}
	post()
}

function getCondition(condition_code)
{
	load()
	var obj = test_info
		for (var k in obj.conditions)
		{
			if (k == condition_code)
			{
            	if (obj.conditions[k] && typeof obj.conditions[k] == "object")
				{
					condition = obj.conditions[k]
				}
				else
				{
					post("error, condition code is not an object")
					return
				}
				
				for (var i in condition)
				{
					if (condition[i] instanceof Array)
					{
					 	condition[i] = condition[i].toString().replace(/,/g, " ")
					}
					outlet(0, i)
					outlet(0, condition[i])
				}
				
			}
				
		}
}

function getTest()
{
	load()
	var obj = test_info
	for (var k in obj)
	{	
			if (k != "conditions")
			{
			if (obj[k] instanceof Array)
			{
		 		obj[k] = obj[k].toString().replace(/,/g, " ")
			}
			outlet(0, k)
			outlet(0, obj[k])
		}
	}
}	



outlets = 1;