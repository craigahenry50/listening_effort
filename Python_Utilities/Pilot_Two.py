import Test_classes as le
import os
root = "D:\\Documents\\Docs\\Imperial\\Listening_Effort\\"

idling = ["_", "idle/Idle.mp4", "_"]

screens = le.Screens(root + "Test_Files\\positions.txt", idling)

sentences = le.SentenceList(18, [x * 15 for x in range(18)], "ASL")

male_dir = root + "Media\\Masking\\male_babble"
male_babble = [os.path.join(male_dir, file) for file in os.listdir(male_dir)]
female_dir = root + "Media\\Masking\\female_babble"
female_babble = [os.path.join(female_dir, file) for file in os.listdir(female_dir)]

around = le.MaskingCondition("around", 8)
around.set_source_positions([[1, 45], [1, 45], [1, 135], [1, 135], [1, 225], [1, 225], [1, 315], [1, 315]])
around.set_audio_files([male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop()])
around.set_video("all_around.mp4")

NSNV = le.TestCondition("NSNV", around, 2, 1, 0, screens)
NSNV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Screen central. No video, no spatialisation"

SNR_increment = "0."

test_one = le.Test("Pilot Two", [NSNV, NSNV, NSNV, NSNV], sentences, SNR_increment)
test_one.output_new()

