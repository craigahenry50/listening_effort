import Test_classes as le
import os

root = "D:\\Documents\\Docs\\Imperial\\Listening_Effort\\"


# Create an array of idling videos for the three screens. "_" Means blank.
idling = ["_", "idle/Idle.mp4", "_"]

# Create a screens instance with a screen positions text file created using the relevant max patch utility and the
#  idling videos
screens = le.Screens(root + "Test_Files\\positions.txt", idling)

# Create a SentenceList instance with 18 sets of sentences, A list of indexes for the first sentence in each set
# (There are 15 in each set) and finally the name of the list "ASL"
sentences = le.SentenceList(18, [x * 15 for x in range(18)], "ASL")

# Create some arrays containing all the available babble files.
male_dir = root + "Media\\Masking\\male_babble"
male_babble = [os.path.join(male_dir, file) for file in os.listdir(male_dir)]
female_dir = root + "Media\\Masking\\female_babble"
female_babble = [os.path.join(female_dir, file) for file in os.listdir(female_dir)]

# Create a masking condition called "around" with 8 masking sources. Position each source. Add an audio file to each
# masking source from the array of babble files created before. Add a masking video
around = le.MaskingCondition("around", 8)
around.set_source_positions([[1, 45], [1, 45], [1, 135], [1, 135], [1, 225], [1, 225], [1, 315], [1, 315]])
around.set_audio_files([male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop()])
around.set_video("all_around.mp4")

# Create some Test conditions, all of which use the around masking condition. Define their condition code, active screen,
# audio only flag, spatialisation flag and include the screen positions. Finally add a description for each
NSNV = le.TestCondition("NSNV", around, 2, 1, 0, screens)
NSNV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Screen central. No video, no spatialisation"
NSV = le.TestCondition("NSV", around, 2, 0, 0, screens)
NSV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Screen central. With video, no spatialisation"
SNV = le.TestCondition("SNV", around, 2, 1, 1, screens)
SNV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Screen central. No video, spatialised"
SV = le.TestCondition("SV", around, 2, 0, 1, screens)
SV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Screen central. With video, spatialised"

# Define the default SNR increment.
SNR_increment = "10."

# Create the test with the Test name, list of conditions, sentences used and the default SNR increment.
test_one = le.Test("Pilot One", [NSNV, NSV, SNV, SV], sentences, SNR_increment)

# Finally output the XML and JSON files
test_one.output()

