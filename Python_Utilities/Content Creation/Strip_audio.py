from moviepy.editor import *
import os
import soundfile as sf

path = "D:\\Documents\\Docs\\Imperial\\Assets\\bkb_new"
audio_path = "D:\\Documents\\Docs\\Imperial\\Listening_Effort\\Media\\BKB_new"

for video in os.listdir(path):
    audio_file = os.path.join(audio_path,  os.path.splitext(video)[0] + ".wav")
    if not os.path.exists(audio_file):
        print(video)
        audio = AudioFileClip(os.path.join(path, video))
        audio_array = audio.to_soundarray(fps=audio.fps)
        sf.write(audio_file, audio_array[:, 0], 44100, subtype='PCM_24', format='WAV')

