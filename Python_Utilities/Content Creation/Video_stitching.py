from moviepy.editor import *

# Load videos to be stitched
vid_one = VideoFileClip("C:\\Users\\craig\\Documents\\Videos\\centre_and_back_right_new.mp4")
vid_two = VideoFileClip("C:\\Users\\craig\\Documents\\Videos\\two_in_front_new.mp4")

# Set the duration of each video
vid_one = vid_one.set_duration(240)
vid_two = vid_two.set_duration(240)


#Crop each video according to the sections you want to keep. For instance here the middle section of video 2 is preserved
# and surrounded by the first and last quarter of video two
crop_vid_one = vid_one.crop(x1=0, y1=0, x2=vid_one.size[0]/4.0, y2=vid_one.size[1])
crop_vid_two = vid_two.crop(x1=vid_two.size[0]/4.0, y1=0, x2=vid_two.size[0]*3.0/4.0, y2=vid_two.size[1])
crop_vid_three = vid_one.crop(x1=vid_one.size[0]*3.0/4.0, y1=0, x2=vid_one.size[0], y2=vid_one.size[1])

#stitch this videos together
vid_new = clips_array([[crop_vid_one, crop_vid_two, crop_vid_three]])

#write the video to a new file
vid_new.write_videofile("C:\\Users\\craig\\Documents\\Videos\\one_in_front_new.mp4")
