import numpy as np
import os
import json
import collections


class Test:
    # Create a test with a title (str), test conditions (array of TestConditions), sentence list (SentenceList),
    #  starting SNR increment (str) and path to the beep audio file (str). If beep is left blank then there is no beep
    def __init__(self, title, test_conditions, sentence_list, starting_snr_inc, beep=""):
        self._title = title
        self._test_conditions = test_conditions
        self._sentences = sentence_list
        self._beep = beep
        self._starting_snr_inc = starting_snr_inc

    def add_condition(self, test_condition):
        self._test_conditions.extend(test_condition)

    # Output a json file to be loaded by the control patch. Any parameters defined in this script which are to be
    # carried out by the control patch should be defined in this file.
    def write_json(self, num_sources, num_masking_sources, output_dir="../Test_Files"):

        # Overall test parameters

        data = collections.defaultdict(dict)
        data["name"] = self._title
        data["n_sources"] = num_sources
        data["n_masking_sources"] = num_masking_sources
        data["n_lists"] = len(self._sentences.indexes)
        data["list_indexes"] = self._sentences.indexes
        data["sentence_text_file"] = self._sentences.text_file
        data["starting_snr_inc"] = self._starting_snr_inc
        data["beep"] = int(self._beep is not "")
        data["conditions"] = collections.defaultdict(dict)

        source_count = int(self._beep is not "")
        maskers = []

        # Per condition parameters

        for iter, condition in enumerate(self._test_conditions):

            if condition.masking_condition.name not in maskers:
                maskers.append(condition.masking_condition.name)
                condition.masking_condition.source_index = source_count
                source_count += num_sources

            data["conditions"][condition.code] = collections.defaultdict(dict)

            data["conditions"][condition.code]["code"] = condition.code
            data["conditions"][condition.code]["description"] = condition.description
            data["conditions"][condition.code]["first_source"] = condition.masking_condition.source_index
            data["conditions"][condition.code]["n_sources"] = condition.masking_condition.get_num_sources()
            data["conditions"][condition.code]["video"] = condition.masking_condition.get_video()
            data["conditions"][condition.code]["audio_only"] = condition.audio_only
            data["conditions"][condition.code]["spatialised"] = condition.spatialised
            data["conditions"][condition.code]["n_screens"] = condition.screens.quantity
            data["conditions"][condition.code]["active_screen"] = condition.active_screen
            data["conditions"][condition.code]["screen_positions"] = condition.screens.positions
            data["conditions"][condition.code]["idling"] = condition.screens.idling

        with open(os.path.join(output_dir, self._title + ".json"), "w") as write_file:
            json.dump(data, write_file, indent=4)

    # Writes the xml for the BiTA to load all the necessary files, indexed in a way which is read by the control patch
    # in the JSON file.

    def write_xml(self, output_dir="../Test_Files"):

        # Write a source to the XML
        def write_source(file, num, pos, name, location, vol, vol_db, slider_pos=45):
            file.write("\t<Source%d_x>%.9f</Source%d_x>\n" % (num, pos[0], num))
            file.write("\t<Source%d_y>%.9f</Source%d_y>\n" % (num, pos[1], num))
            file.write("\t<Source%d_z>%.9f</Source%d_z>\n" % (num, 0, num))
            file.write("\t<Source%d_vol>%.9f</Source%d_vol>\n" % (num, vol, num))
            file.write("\t<Source%d_vol_dB>%.9f</Source%d_vol_dB>\n" % (num, vol_db, num))
            file.write("\t<Source%d_sliderPosition>%d</Source%d_sliderPosition>\n" % (num, slider_pos, num))
            file_path = location + "/" + name
            file.write("\t<Source_%d_filePath>%s</Source_%d_filePath>\n" % (num, file_path, num))

        sentence_dir = "../Media/" + self._sentences.text_file
        xml_filename = os.path.join(output_dir, self._title + ".xml")
        xml = open(xml_filename, "w+")
        maskers = []
        files = []
        positions = []

        if self._beep is not "":
            source_count = 1
            beepPresent = True
        else:
            source_count = 0
            beepPresent = False


        for condition in self._test_conditions:
            masker = condition.masking_condition
            num_sources = masker.get_num_sources()

            # Make sure a masking condition isn't repeated to create duplicate sources
            if masker.name not in maskers:
                maskers.append(masker.name)
                files.extend(masker.get_audio_files())
                positions.extend(masker.get_source_positions())
                masker.source_index = source_count
                source_count += num_sources

        num_masking_sources = source_count
        source_count += len(os.listdir(sentence_dir))

        xml.write("<BinauralApp>\n"
                  "\t<FrameSize>%d</FrameSize>\n"
                  "\t<ListenerPosX>0.000000000</ListenerPosX>\n"
                  "\t<ListenerPosY>0.000000000</ListenerPosY>\n"
                  "\t<ListenerPosZ>0.000000000</ListenerPosZ>\n"
                  "\t<ListenerOrX>-0.000000000</ListenerOrX>\n"
                  "\t<ListenerOrY>-0.000000000</ListenerOrY>\n"
                  "\t<ListenerOrZ>0.000000000</ListenerOrZ>\n"
                  "\t<ListenerOrW>1.000000000</ListenerOrW>\n"
                  "\t<Platform>Windows</Platform>\n"
                  "\t<OSCListenPort>12300</OSCListenPort>\n"
                  "\t<ReverbOrder>%s</ReverbOrder>\n"
                  "\t<NumSources>%d</NumSources>\n" % (256, "3D", source_count))
        source = 0

        gain = 1
        if beepPresent:
            cart = spherical_2_cartesian(1, 0, 0)
            filepath, filename = os.path.split(self._beep)
            write_source(xml, source, cart, filename, get_rel_path(output_dir, filepath), gain, 20 * np.log10(gain))
            source += 1
            xml.write("\n")

        for i, filename in enumerate(files):
            x, y = positions[i]

            cart = spherical_2_cartesian(1, x, y)
            filepath, filename = os.path.split(filename)
            write_source(xml, source, cart, filename, get_rel_path(output_dir, filepath), gain, 20 * np.log10(gain))
            source += 1
            xml.write("\n")

        for filename in os.listdir(sentence_dir):
            cart = spherical_2_cartesian(1, 0, 0)
            write_source(xml, source, cart, filename, get_rel_path(output_dir, sentence_dir), gain, 20 * np.log10(gain))
            source += 1
            xml.write("\n")

        xml.write("</BinauralApp>")
        xml.close()
        return source_count, num_masking_sources

    def output(self, output_dir="../Test_Files"):
        sources, masking_sources = self.write_xml(output_dir)
        self.write_json(sources, masking_sources, output_dir)


# Container class for info regarding the sentence list being used.
class SentenceList:
    n_sets = 0
    indexes = []
    text_file = ""

    def __init__(self, num_sets, indexes, text_file):
        self.n_sets = num_sets
        self.indexes = indexes
        self.text_file = text_file


# Class containing info about a test condition. Made up of parameters that might need to be defined for each condition
class TestCondition:
    code = ""
    description = ""
    masking_condition = 0
    active_screen = 0
    audio_only = 0
    spatialised = 0
    screens = []

    def __init__(self, code, masking_condition, active_screen, audio_only, spatialised, screens):
        self.code = code
        self.masking_condition = masking_condition
        self.active_screen = active_screen
        self.audio_only = audio_only
        self.spatialised = spatialised
        self.screens = screens


# Class containing info regarding screen set ups in a particular test condition. Loaded from a text file. Screens should
# be positioned using the max utility patch which creates the text file in the correct format.
class Screens:
    positions = []
    positions_str = ""
    idling = []
    idling_str = ""
    quantity = 0

    def __init__(self, text_positions, idling):
        text_file = open(text_positions, "r")
        for line in text_file:
            for pos in line.split():
                self.positions.append(pos)
            line = line.replace('\r', '')
            line = line.replace('\n', '')
            self.positions_str += line
            self.quantity += 1

        for idle in idling:
            self.idling_str += idle + " "
        self.idling = idling


# Class containing info re a specific masking condition. This most importantly includes the video and audio files.
class MaskingCondition:
    _num_sources = 0
    _sources = 0
    _files = []
    _video = ""
    source_index = []
    name = ""

    def __init__(self, name,  num_sources):
        self.name = name
        self._num_sources = num_sources
        self._sources = np.zeros((num_sources, 2))
        self._files = ["" for _ in range(num_sources)]


    def get_num_sources(self):
        return self._num_sources

    def set_source_positions(self, positions):
        if self._num_sources == 1:
            positions = [positions]
        if len(positions) != self._num_sources:
            print("Number of positions does not match the number of sources")
            return

        for i, position in enumerate(positions):
            self._sources[i] = position

    def get_source_positions(self):
        return self._sources

    def set_audio_files(self, files):
        if type(files) is str:
            files = [files]
        for i in range(self._num_sources):
            if len(files) == 1:
                self._files[i] = files[0]
            else:
                self._files[i] = files[i]

    def get_audio_files(self):
        return self._files

    def set_video(self, video):
        self._video = video

    def get_video(self):
        return self._video


# Phi is the angle from the lateral plane, theta is the azimuth and anti-clockwise
def spherical_2_cartesian(r, phi, theta):
    theta = np.deg2rad(theta)
    phi = np.deg2rad(phi)

    x = r * np.cos(phi) * np.cos(theta)
    y = r * np.cos(phi) * np.sin(theta)
    z = r * np.sin(phi)

    return [x, y, z]


def get_rel_path(source, destination):
    out = os.path.relpath(destination, source)
    output = out.replace('\\', '/')
    output = './' + output
    return output


