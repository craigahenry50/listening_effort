import cv2
import math
import matplotlib.pyplot as plt
from PIL import Image, ImageStat

def brightness5(img):
    im = Image.fromarray(img)
    stat = ImageStat.Stat(im)
    gs = (math.sqrt(0.241 * (r ** 2) + 0.691 * (g ** 2) + 0.068 * (b ** 2))
          for r, g, b in list(im.getdata()))
    return sum(gs) / stat.count[0]

def brightness4(img):
    im = Image.fromarray(img)
    stat = ImageStat.Stat(im)
    r, g, b = stat.rms
    return math.sqrt(0.241 * (r ** 2) + 0.691 * (g ** 2) + 0.068 * (b ** 2))

vidcap = cv2.VideoCapture('D:\\Videos\\Captures\\ListeningEffortPlayer 2020-03-26 15-32-13.mp4')
success,image = vidcap.read()
brightness = []
count = 0
while success:
    brightness.append(brightness4(image))
    count += 1
    success,image = vidcap.read()
    print('Read a new frame: ', success)

plt.plot(brightness)
plt.show()